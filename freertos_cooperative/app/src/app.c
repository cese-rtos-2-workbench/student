/*
 * Copyright (c) 2023 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 */

/********************** inclusions *******************************************/

#include "main.h"
#include "cmsis_os.h"
#include "logger.h"
#include "dwt.h"
#include "board.h"

/********************** macros and definitions *******************************/

#define TASK_PERIOD_MS_         (1000)
#define MSG_TICK_               ("tick")

#define N_TASKS_                (6)
#define TASK_STACK_SIZE_        (128)
#define START_TIME_             (10)

/********************** internal data declaration ****************************/

typedef void (*task_handler_t)(void);

typedef struct
{
    task_handler_t task_handler;
    TickType_t delay;
    TickType_t period;
} task_descriptor_t;

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

task_descriptor_t descriptors_[N_TASKS_];

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static void cooperative_task_(void *argument)
{
  task_descriptor_t *pdescriptor = (task_descriptor_t*)argument;
  vTaskDelay((TickType_t)((pdescriptor->delay + START_TIME_) / portTICK_PERIOD_MS));
  while (true)
  {
    pdescriptor->task_handler();
    vTaskDelay((TickType_t)(pdescriptor->period / portTICK_PERIOD_MS));
  }
}

static void create_cooperative_task_(
    task_descriptor_t *pdescriptor,
    const char *const task_name,
    task_handler_t task_handler,
    TickType_t delay,
    TickType_t period)
{
  BaseType_t status;

  pdescriptor->task_handler = task_handler;
  pdescriptor->delay = delay;
  pdescriptor->period = period;

  status = xTaskCreate(cooperative_task_, task_name, TASK_STACK_SIZE_, (void*)pdescriptor, tskIDLE_PRIORITY, NULL);
  while (pdPASS != status)
  {
    // error
  }
}

static void task_1_(void)
{
  LOGGER_LOG("T1\n");
}

static void task_2_(void)
{
  LOGGER_LOG("T2\n");
}

static void task_3p1_(void)
{
  LOGGER_LOG("T3p1\n");
}

static void task_3p2_(void)
{
  LOGGER_LOG("T3p2\n");
}

static void task_3p3_(void)
{
  LOGGER_LOG("T3p3\n");
}

static void task_3p4_(void)
{
  LOGGER_LOG("T3p4\n");
}

/********************** external functions definition ************************/

void app_init(void)
{
  LOGGER_LOG("tasks init\n");

  create_cooperative_task_(&(descriptors_[0]), "T1",   task_1_,    0 +  0*100, 20*100);
  create_cooperative_task_(&(descriptors_[1]), "T2",   task_2_,    4 +  0*100, 40*100);
  create_cooperative_task_(&(descriptors_[2]), "T3.1", task_3p1_, 14 +  0*100, 80*100);
  create_cooperative_task_(&(descriptors_[3]), "T3.2", task_3p2_,  4 + 20*100, 80*100);
  create_cooperative_task_(&(descriptors_[4]), "T3.2", task_3p3_, 14 + 40*100, 80*100);
  create_cooperative_task_(&(descriptors_[5]), "T3.4", task_3p4_,  4 + 60*100, 80*100);

  cycle_counter_init();
}

/********************** end of file ******************************************/
